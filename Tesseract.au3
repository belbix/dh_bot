#include-once

Global $h_TESSDll = 0
Global $h_LEPTDll = 0

Func _TessStart()

   $h_TESSDll = DllOpen('libtesseract302.dll')
   If $h_TESSDll = -1 Then
	  MsgBox(16, "ERROR!", "libtesseract302.dll not found!")
	  Exit
   EndIf

   $h_LEPTDll = DllOpen('liblept168.dll')
   If $h_LEPTDll = -1 Then
	  MsgBox(16, "ERROR!", "libtesseract302.dll not found!")
	  Exit
   EndIf
EndFunc ;==>_TessStart

Func _TessShutdown()
   DllClose($h_LEPTDll)
   DllClose($h_TESSDll)
EndFunc ;==>_TessShutdown

#Region Public Functions libtesseract302.dll

Func _TessBaseAPICreate()

   $a_Ret = DllCall($h_TESSDll, 'ptr:cdecl', 'TessBaseAPICreate')
   If Not $a_Ret[0] Then
	  Return SetError(1, 0, 0)
   EndIf

   Return SetError(0, 0, $a_Ret[0])
EndFunc ;==>_TessBaseAPICreate

Func _TessBaseAPIDelete($h_Tess)
   DllCall($h_TESSDll, 'none:cdecl', 'TessBaseAPIDelete', 'ptr', $h_Tess)
EndFunc ;==>_TessBaseAPIDelete

Func _TessBaseAPIEnd($h_Tess)
   DllCall($h_TESSDll, 'none:cdecl', 'TessBaseAPIEnd', 'ptr', $h_Tess)
EndFunc ;==>_TessBaseAPIEnd

Func _TessBaseAPIGetUTF8Text($h_Tess)

   $a_Ret = DllCall($h_TESSDll, 'str:cdecl', 'TessBaseAPIGetUTF8Text', 'ptr', $h_Tess)
   If Not $a_Ret[0] Then
	  Return SetError(1, 0, 0)
   EndIf
   Return SetError(0, 0, $a_Ret[0])
EndFunc ;==>_TessBaseAPIGetUTF8Text

Func _TessDeleteText($s_Text)
   DllCall($h_TESSDll, 'none:cdecl', 'TessDeleteText', 'str', $s_Text)
EndFunc ;==>_TessDeleteText

Func _TessBaseAPIInit3($h_Tess, $s_DataPath = "", $s_Language = "eng")

   $a_Ret = DllCall($h_TESSDll, 'int:cdecl', 'TessBaseAPIInit3', 'ptr', $h_Tess, 'str', $s_DataPath, 'str', $s_Language)
   If $a_Ret[0] Then
	  Return SetError(1, 0, 0)
   EndIf
   Return SetError(0, 0, $a_Ret[0])
EndFunc ;==>_TessBaseAPIInit3

Func _TessBaseAPIRecognizet($h_Tess, $h_Monitor = 0)

   $a_Ret = DllCall($h_TESSDll, 'int:cdecl', 'TessBaseAPIRecognize', 'ptr', $h_Tess, 'ptr', $h_Monitor)
   If $a_Ret[0] Then
	  Return SetError(1, 0, 0)
   EndIf
   Return SetError(0, 0, $a_Ret[0])
EndFunc ;==>_TessBaseAPIRecognizet

Func _TessBaseAPISetImage2($h_Tess, $h_Pix)
   $a_Ret = DllCall($h_TESSDll, 'none:cdecl', 'TessBaseAPISetImage2', 'ptr', $h_Tess, 'ptr', $h_Pix)
EndFunc ;==>_TessBaseAPISetImage2

#EndRegion Public Functions libtesseract302.dll

#Region Public Functions liblept168.dll

Func _PixRead($s_FileName)

   $a_Ret = DllCall($h_LEPTDll, 'ptr:cdecl', 'pixRead', 'str', $s_FileName)
   If Not $a_Ret[0] Then
	  Return SetError(1, 0, 0)
   EndIf
   Return SetError(0, 0, $a_Ret[0])
EndFunc ;==>_PixRead

Func _PixFreeData($h_Pix)

   $a_Ret = DllCall($h_LEPTDll, 'uint:cdecl', 'pixFreeData', 'ptr', $h_Pix)
   If Not $a_Ret[0] Then
	  Return SetError(1, 0, 0)
   EndIf
   Return SetError(0, 0, $a_Ret[0])
EndFunc ;==>_PixFreeData

#EndRegion Public Functions liblept168.dll