#include <ScreenCapture.au3>
#include <Date.au3>
#include "BmpSearch.au3"
#include <GUIConstantsEx.au3>
#include <WindowsConstants.au3>
#include 'Tesseract.au3'

_GDIPlus_Startup()
GUISetState()
Global $kyski = 1
Global $rassa = ""
Global $score_min = 2300
Global $score_max = 3300
Global $activ_sbor_kyski = 1
Global $BgActive = 0, $sbor = 0, $count_main = 0, $i = 0, $numFriend = 10
Global $array_friend[$numFriend]
$array_friend[1] = "MATRIX"
$array_friend[2] = "MATRIXReload"
$array_friend[3] = "Alienware"
$array_friend[4] = "Holyinquisite"
$array_friend[5] = "AcrapnH" ;AsgardII
$array_friend[6] = "Xcrapl:" ;Asgard
$array_friend[7] = ""
$array_friend[8] = "MATRIX"

HotKeySet("{F6}", "_AtackBg")
HotKeySet("{F5}", "_BgActive")
HotKeySet("{F4}", "_Terminate")
HotKeySet("{F3}", "test")
Func test()
	$kysok = findImg("img/death_kart.bmp", "", 179, 544, 254, 587)
	If $kysok <> 0 Then
		$tmp = $kysok[1][3]
		ConsoleWrite($tmp & @CRLF)
	EndIf

EndFunc   ;==>test

Func _Terminate()
	Exit 0
EndFunc   ;==>_Terminate

Func _SborActive()
	If $kyski == 0 Then
		$kyski = 1
	Else
		$kyski = 0
	EndIf
EndFunc   ;==>_SborActive

Func _BgActive()
	If $BgActive == 0 Then
		$BgActive = 1
	Else
		$BgActive = 0
	EndIf
EndFunc   ;==>_BgActive

Func findImg($dirBmp, $dirScr, $1x, $1y, $2x, $2y)
	If $dirScr == "" Then
		$tmpImg = _ScreenCapture_Capture($dirScr, $1x, $1y, $2x, $2y, False)
	Else
		_ScreenCapture_Capture($dirScr, $1x, $1y, $2x, $2y, False)
		$tmpScr = _GDIPlus_BitmapCreateFromFile($dirScr)
		$tmpImg = _GDIPlus_BitmapCreateHBITMAPFromBitmap($tmpScr)
	EndIf
	$hBmp = _GDIPlus_BitmapCreateFromFile($dirBmp)
	$hFind = _GDIPlus_BitmapCreateHBITMAPFromBitmap($hBmp)
	$tmp = _BmpSearch($tmpImg, $hFind, 1)
	If $dirScr <> "" Then
		_WinAPI_DeleteObject($tmpScr)
	EndIf
	_WinAPI_DeleteObject($tmpImg)
	_WinAPI_DeleteObject($hFind)
	_WinAPI_DeleteObject($hBmp)
	Return $tmp
EndFunc   ;==>findImg

Func findText($dir)
	_TessStart()
	$hTess = _TessBaseAPICreate()
	If @error Then
		ConsoleWrite(StringFormat('--> ERROR Base API Create\n'))
	EndIf
	_TessBaseAPIInit3($hTess)
;~         _TessBaseAPIInit3($hTess, '', 'rus')
	If @error Then
		ConsoleWrite(StringFormat('--> ERROR initialising tesseract\n'))
	EndIf

	$hPix = _PixRead($dir)
	_TessBaseAPISetImage2($hTess, $hPix)
	_TessBaseAPIRecognizet($hTess)
	$tmp = _TessBaseAPIGetUTF8Text($hTess)
	$tmp = StringStripWS($tmp, 8)
	_TessBaseAPIEnd($hTess)
	_TessBaseAPIDelete($hTess)
	_PixFreeData($hPix)
	Return $tmp
EndFunc   ;==>findText

Func skyArena()


	Sleep(1000)
	MouseClick("left", 719, 194) ;sky
	Sleep(5000)
	If findImg("img/null_sky.bmp", "", 171, 67, 284, 98) <> 0 Then
		ConsoleWrite("--> Arena 0")
		MouseClick("left", 885, 80) ;exit
		Sleep(2000)
	Else
		Sleep(2000)
		MouseClick("left", 647, 558) ;koloda
		Sleep(2000)
		MouseClick("left", 585, 90) ;koloda 3
		Sleep(2000)
		MouseClick("left", 885, 80) ;exit
		Sleep(2000)
		MouseClick("left", 830, 507) ;fight

		For $i = 1 To 5 Step 1
			Sleep(2000)
			MouseClick("left", 510, 496) ;elit
			Sleep(10000)
			MouseClick("left", 854, 218) ;avto
			Sleep(2000)
			MouseClick("left", 853, 396) ;skip
			Sleep(3000)
			MouseClick("left", 559, 353) ;close win/lose
			Sleep(2000)
			MouseClick("left", 371, 447) ;buy no

		Next


		Sleep(2000)
		MouseClick("left", 885, 80) ;exit
		Sleep(2000)
		MouseClick("left", 647, 558) ;koloda
		Sleep(2000)
		MouseClick("left", 778, 93) ;koloda 4
		Sleep(2000)
		MouseClick("left", 885, 80) ;exit
		Sleep(2000)
		MouseClick("left", 885, 80) ;exit
	EndIf



EndFunc   ;==>skyArena

Func _LoadDh()
;~ 	If WinExists("������������ �����") <> 0 Then
	WinClose("������������")
	MouseClick("left", 855, 443) ;run dh
;~ 	EndIf

	If WinExists("BlueStacks App") == 0 Then
		Run('"C:\Program Files (x86)\BlueStacks\HD-StartLauncher.exe"')
		Sleep(15000)
		_FindAndClick("DH link","img/dh_logo.bmp",0, 0, 1028, 728)
;~ 		$coordDhLogo = 0
;~ 		While $coordDhLogo == 0
;~ 			$coordDhLogo = findImg("img/dh_logo.bmp", "", 0, 0, 1028, 728)
;~ 			ConsoleWrite("find dh logo" & @CRLF)
;~ 			If $coordDhLogo <> 0 Then
;~ 				While $coordDhLogo <> 0
;~ 					ConsoleWrite("click dh logo" & @CRLF)
;~ 					MouseClick("left", $coordDhLogo[1][2], $coordDhLogo[1][3]) ;run dh
;~ 					Sleep(3000)
;~ 					$coordDhLogo = findImg("img/dh_logo.bmp", "", 0, 0, 1028, 728)
;~ 				WEnd
;~ 				$coordDhLogo = 1
;~ 				ConsoleWrite("--> END find dh logo" & @CRLF)
;~ 			EndIf

;~ 			Sleep(3000)
;~ 		WEnd
;~         Sleep(30000)
        $coordStartLogo = 0
		While $coordStartLogo == 0
			$coordStartLogo = findImg("img/start_logo.bmp", "", 824, 221, 915, 283)
			ConsoleWrite("find start logo" & @CRLF)
			If $coordStartLogo <> 0 Then
				While $coordStartLogo <> 0
					ConsoleWrite("click start logo" & @CRLF)
					MouseClick("left", $coordStartLogo[1][2], $coordStartLogo[1][3]) ;close first window
					Sleep(3000)
					$coordStartLogo = findImg("img/start_logo.bmp", "", 824, 221, 915, 283)
				WEnd
				$coordStartLogo = 1
				ConsoleWrite("--> END find start logo" & @CRLF)
			EndIf

			Sleep(3000)
		WEnd
;~ 		MouseClick("left", 879, 253) ;close first window
	EndIf
EndFunc   ;==>_LoadDh

Func _FindAndClick($log_str, $img_adr,$x1,$y1,$x2,$y2)
	ConsoleWrite("--> Start find "&$log_str & @CRLF)

$coord = 0
		While $coord == 0
			$coord = findImg($img_adr, "", $x1,$y1,$x2,$y2)
			ConsoleWrite("Find "&$log_str & @CRLF)
			If $coord <> 0 Then
				While $coord <> 0
					ConsoleWrite("Click "&$log_str & @CRLF)
					MouseClick("left", $coord[1][2], $coord[1][3]) ;close first window
					Sleep(3000)
					$coord = findImg($img_adr, "", $x1,$y1,$x2,$y2)
				WEnd
				$coord = 1
				ConsoleWrite("--> END find "&$log_str & @CRLF)
			EndIf

			Sleep(3000)
		WEnd
EndFunc

Func kyski()

	_LoadDh()

	WinActivate("Blue")
	If findImg("img/main_img.bmp", "", 0, 0, 1028, 728) <> 0 Then

		;skyArena()
		If $activ_sbor_kyski == 1 Then
			Sleep(1000)
			MouseClick("left", 457, 234) ;guild
			Sleep(2000)
			MouseClick("left", 421, 92) ;gi map
			Sleep(2000)
;~ 		MouseClick("left", 817, 349);glava 2
			MouseClick("left", 820, 565);glava 4
;~         MouseClickDrag($MOUSE_CLICK_LEFT, 322, 406, 327, 282) ;��������� ����
;~ 		MouseClickDrag($MOUSE_CLICK_LEFT, 322, 406, 327, 282) ;��������� ����
;~ 		MouseClickDrag($MOUSE_CLICK_LEFT, 322, 406, 327, 282) ;��������� ����
;~ 		MouseClick("left", 809, 537);glava 6
			Sleep(2000)
			MouseClick("left", 543, 80);dobicha
			$i = 0
			While $sbor == 0
				Sleep(2000)
				$kysok = findImg("img/abb.bmp", "", 0, 0, 1028, 728)
				If $kysok <> 0 Then
					MouseClick("left", 756, $kysok[1][3] + 15);vibor kyska
					$sbor = 1
					$kysok = 0
					Sleep(2000)
				Else
					MouseClickDrag($MOUSE_CLICK_LEFT, 322, 406, 327, 282)
				EndIf
				Sleep(2000)
				$i += 1
				If $i > 30 Then
					$sbor = 1
					$kysok = 0
				EndIf
			WEnd
			$kysok = 0
			$sbor = 0
			If findImg("img/yes_no.bmp", "", 0, 0, 1028, 728) <> 0 Then
				MouseClick("left", 390, 462)
			EndIf
;~ 		MouseClick("left", 359, 460);klik no
			Sleep(1000)
			Send("{ESC}")
			Sleep(1000)
			Send("{ESC}")
			Sleep(1000)
			Send("{ESC}")
		EndIf
		WinClose("BlueStacks App")
		$count_main = 0



		Sleep(1000 * 60 * 15)

	Else
		$count_main += 1
		If $count_main > 30 Then
			WinClose("BlueStacks App")
		EndIf

		If findImg("img/main_exit.bmp", "", 0, 0, 1028, 728) <> 0 Then
			MouseClick("left", 754, 199)
		Else
			Send("{ESC}")
			Sleep(1000)
		EndIf
	EndIf


EndFunc   ;==>kyski


Func _BG()

	For $countClick = 1 To 6

		Sleep(1000)
		If $countClick == 1 Then
			MouseClick("left", 215, 353) ;karavan 1
		EndIf
		If $countClick == 2 Then
			MouseClick("left", 219, 451) ;karavan 2
		EndIf
		If $countClick == 3 Then
			MouseClick("left", 377, 348) ;karavan 3
		EndIf
		If $countClick == 4 Then
			MouseClick("left", 555, 346) ;karavan 4
		EndIf
		If $countClick == 5 Then
			MouseClick("left", 588, 416) ;karavan 5
		EndIf
		If $countClick == 6 Then
			MouseClick("left", 730, 378) ;karavan 6
		EndIf

		Sleep(1000)
		$dirScrEnemyName = "tmp_img/enemy_name.tif"
		_ScreenCapture_Capture($dirScrEnemyName, 475, 235, 610, 256, False)
		$name_karavan = findText($dirScrEnemyName)
		ConsoleWrite("--> name karavan = " & $name_karavan & @CRLF)

		$dirScrEnemyScore = "tmp_img/enemy_score.tif"
		_ScreenCapture_Capture($dirScrEnemyScore, 492, 268, 567, 289, False)
		$score_karavan = findText($dirScrEnemyScore)
		$tmp = $score_karavan
		$tmp = StringStripWS(StringReplace($tmp, "S", "5"), 8)
		$tmp = StringReplace($tmp, "Z", "2")
		$tmp = StringReplace($tmp, "B", "8")
		$tmp = StringReplace($tmp, "O", "0")
		$score_karavan = $tmp
		ConsoleWrite("Score karavan = " & $score_karavan & @CRLF)

		$dirScrEnemyKyr = "tmp_img/enemy_kyr.tif"
		_ScreenCapture_Capture($dirScrEnemyKyr, 386, 294, 446, 316, False)
		$kyr_karavan = findText($dirScrEnemyKyr)
		$tmp = $kyr_karavan
		$tmp = StringStripWS(StringReplace($tmp, "S", "5"), 8)
		$tmp = StringReplace($tmp, "Z", "2")
		$tmp = StringReplace($tmp, "B", "8")
		$tmp = StringReplace($tmp, "O", "0")
		$kyr_karavan = $tmp
		ConsoleWrite("Kyrier karavan = " & $kyr_karavan & @CRLF)
		$not_atack = 0
		For $countFriend = 1 To $numFriend - 1
			If $name_karavan == $array_friend[$countFriend] Then
				$not_atack = 1
				ConsoleWrite("Drujishe! Ne atakyem" & @CRLF)
			EndIf
		Next

		$MO_atack = ($score_karavan - ($kyr_karavan * 1000)) + 1000
		ConsoleWrite("MO = " & $MO_atack & @CRLF)
		If $not_atack == 0 And $MO_atack > $score_min And $MO_atack < $score_max Then
			MouseClick("left", 385, 461) ;atack!
			ConsoleWrite("Atack!!! " & $name_karavan & @CRLF)
			$countClick = 1
			_AtackBg()
		Else
			Sleep(1000)
			If findImg("img/kyr_select.bmp", "", 494, 421, 732, 485) <> 0 Then
				ConsoleWrite("skip" & @CRLF)
				MouseClick("left", 620, 461) ;close select
			EndIf
			If $countClick == 6 Then
				Sleep(1000)
				MouseClick("left", 863, 450) ;refresh
				Sleep(2000)
			EndIf
		EndIf
	Next
EndFunc   ;==>_BG

Func _AtackBg()
	ConsoleWrite($rassa & @CRLF)
	Sleep(500)
	MouseClick("left", 423, 361) ;koloda
	Sleep(200)
	If $rassa == "" Then
		$rassa = "human"
	EndIf

	MouseClick("left", 781, 359) ;filter
	Sleep(200)
	MouseClick("left", 724, 356) ;po yrovnu
	Sleep(200)
	If $rassa == "human" Then
		MouseClick("left", 368, 229) ;human
		Sleep(200)
	EndIf
	If $rassa == "elf" Then
		MouseClick("left", 382, 292)
		Sleep(200)
	EndIf
	If $rassa == "nean" Then
		MouseClick("left", 375, 358)
		Sleep(200)
	EndIf
	If $rassa == "morti" Then
		MouseClick("left", 385, 418)
		Sleep(200)
	EndIf
	MouseClick("left", 657, 568) ;filter ok
	Sleep(200)
	If findImg("img/death_kart.bmp", "", 179, 544, 254, 587) == 0 Then
		MouseClick("left", 210, 566) ;click kart 1
		Sleep(100)
		MouseClick("left", 328, 566) ;click kart 2
		Sleep(100)
		MouseClick("left", 453, 566) ;click kart 3
		Sleep(100)
		MouseClick("left", 573, 566) ;click kart 4
		Sleep(100)
		MouseClick("left", 689, 566) ;click kart 5
		Sleep(100)
		MouseClick("left", 809, 566) ;click kart 6
		Sleep(100)
		MouseClickDrag($MOUSE_CLICK_LEFT, 826, 495, 196, 470) ;��������� vpravo
		Sleep(100)
		MouseClick("left", 210, 566) ;click kart 1
		Sleep(100)
		MouseClick("left", 328, 566) ;click kart 2
		Sleep(100)
		MouseClick("left", 453, 566) ;click kart 3
		Sleep(100)
		MouseClick("left", 573, 566) ;click kart 4
		Sleep(100)
	EndIf
	MouseClick("left", 578, 360) ;hero
	Sleep(500)
	MouseClick("left", 781, 359) ;filter
	Sleep(200)
	If $rassa == "human" Then
		MouseClick("left", 368, 229) ;human
	EndIf
	If $rassa == "elf" Then
		MouseClick("left", 382, 292)
	EndIf
	If $rassa == "nean" Then
		MouseClick("left", 375, 358)
	EndIf
	If $rassa == "morti" Then
		MouseClick("left", 385, 418)
	EndIf
	Sleep(100)
	MouseClick("left", 657, 568) ;filter ok
	Sleep(500)
	MouseClick("left", 210, 566) ;click kart 1
	Sleep(100)
	MouseClick("left", 824, 187) ;v atacky!
;~ 	ConsoleWrite("v atacky!" & @CRLF)
	Sleep(200)
	If findImg("img/ok_v_centre.bmp", "", 384, 431, 651, 498) <> 0 Then
		MouseClick("left", 501, 460) ;ok
		Sleep(200)
		If $rassa == "human" Then
			$rassa = "elf"
		Else
			If $rassa == "elf" Then
				$rassa = "nean"
			Else
				If $rassa == "nean" Then
					$rassa = "morti"
				Else
					If $rassa == "morti" Then
						$rassa = "human"
					EndIf
				EndIf
			EndIf
		EndIf
		MouseClick("left", 884, 81) ;close win/lose
		Sleep(2000)
	Else
		Sleep(2000)
		If findImg("img/no_celey1.bmp", "", 454, 381, 599, 416) == 0 Then
			Sleep(15000)
			MouseClick("left", 854, 218) ;avto
			Sleep(2000)
			MouseClick("left", 853, 396) ;skip
			Sleep(3000)
			MouseClick("left", 884, 81) ;close win/lose
			Sleep(2000)
		Else
			MouseClick("left", 854, 218) ;prst
		EndIf
	EndIf

EndFunc   ;==>_AtackBg

While 1

	If $kyski == 1 Then
		kyski()
	EndIf

	If $BgActive == 1 Then
		_BG()
	EndIf
WEnd
