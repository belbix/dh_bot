;===============================================================================
; Function Name:    findBMP
; Description:   Finds a bitmap (.BMP) in another BMP file (other formats PNG and TIFF work also other formats less usefull GIF, JPEG, Exif, WMF, and EMF should work)
; Syntax:        findBMP($BMP1, $BMP2, $MatchType=TRUE)
;
; Parameter(s):  $BMP1           = Filename of bitmap to search in
;                $BMP2           = Filename of bitmap to search for
;                $MatchType     = c24RGBFullMatch, c24RGBPartialMatch, c16RGBFullMatch, c16RGBPartialMatch
;
; Return Value(s): On Success: = Returns Array List
;                On Failure: = @error 1 (Control was found but there was an error with the DLLCall)
;
; Author(s):        JunkEW
;
; Note(s):
;               * Its never an exact match even with TRUE as last few bits are disposed in algorithm and lines below
;                are not checked under assumption that those are 99.99% of the time correct
;            * locking bits overview http://www.bobpowell.net/lockingbits.htm
; ToDo:
;               * Transparency (when search letters on a different background) http://www.winprog.org/tutorial/transparency.html
;               * Match quicker by doing a bitblt with srcinvert when first line is found (instead of checking line by line)
;               * $BMP1 and $BMP2 to be HBITMAP handle as input instead of filenames (will make searching within partial screen easier)
; Example(s):
;
;===============================================================================

Const $c24RGBFullMatch = 1 ;Load as 24 bits and full match
Const $c24RGBPartialMatch = 2 ;Load as 24 bits and partial match
Const $c16RGBFullMatch = 3 ;Load as 16 bits and full match
Const $c16RGBPartialMatch = 4 ;Load as 16 bits and partial match
Const $bDebug = False
Const $cMatchLinePercentage = 0.20 ;Minimum matching, put on 1 if exact matching is needed on all lines
Global $result_tmp[2][9]
Global $arrayRGB[3]
Global $limit = 3

Func findBMP($BMP1, $BMP2, $max_x, $max_y, $MatchType = $c24RGBFullMatch)
	Dim $fLine[1];Line number of found line(s), redimmed when second picture size is known
	Dim $BMP1Data = "", $BMP1Width = 0, $BMP1Height = 0, $BMP1LineWidth = 0;
	Dim $BMP2Data = "", $BMP2Width = 0, $BMP2Height = 0, $BMP2LineWidth = 0
	Dim $foundAt = "", $matchPossible = False, $matchedLines = 0, $foundAtLeft = -1, $foundAtTop = -1
	Dim $bestMatchLine = -1, $HighestMatchingLines = -1; For knowing when no match is found where best area is
	Dim $iPos = 1;
	Dim $imgBytes, $avData;
	Global $res_max_x = $max_x
	Global $res_max_y = $max_y

	Local $iFuzzyDist, $searchFor, $iAbove, $bMatchPossible, $aboveLine
	Local $j, $imgBits
	Local $tDebug

	If ($MatchType = $c24RGBFullMatch) Or ($MatchType = $c24RGBPartialMatch) Then
		$imgBytes = 3
	Else
		$imgBytes = 2
	EndIf

	; Load the bitmap to search in
	GetImage($BMP1, $BMP1Data, $BMP1Width, $BMP1Height, $BMP1LineWidth, 0, $imgBytes)
	$BMP1Data = StringMid(String($BMP1Data), 3)

	; Load the bitmap to find
	GetImage($BMP2, $BMP2Data, $BMP2Width, $BMP2Height, $BMP2LineWidth, 1, $imgBytes)
	;Make it strings to be able to use string functions for searching
	$BMP2Data = StringMid(String($BMP2Data), 3)

	;For reference of line where in BMP2FindIn a line of BMP2Find was found
	If $BMP2Height = 0 Then
		SetError(1, 0, 0)
		Return False
	EndIf

	ReDim $fLine[$BMP2Height]

	;If exact match check every 1 line else do it more fuzzy (as most likely other lines are unique)
	If ($MatchType = $c24RGBFullMatch) Or ($MatchType = $c16RGBFullMatch) Then
		$iFuzzyDist = 1
	Else
		;Check fuzzy every 10% of lines
		$iFuzzyDist = Ceiling(($BMP2Height * 0.1))
	EndIf

;~ 	$begin = TimerInit()
	;Look for each line of the bitmap if it exists in the bitmap to find in
;~ Split bitmap to search in lines
;~  $avData=stringregexp($BMP2Data, ".{1," & $BMP2lineWidth & "}+", 3)

	;########################################################################################################################
	$aScrlenght = Int(StringLen($BMP1Data) / 6)
	ConsoleWrite("StringLen($BMP1Data)=" & $aScrlenght & @CRLF)
	Global $arrayScr[$aScrlenght][3]
	For $k = 0 To $aScrlenght - 1 Step 1
		;����������� ������� ��������� �� �������
		$arrayScr[$k][0] = Dec(StringMid($BMP1Data, ($k * 6) + 1, 2))
		$arrayScr[$k][1] = Dec(StringMid($BMP1Data, ($k * 6) + 3, 2))
		$arrayScr[$k][2] = Dec(StringMid($BMP1Data, ($k * 6) + 5, 2))
	Next
	ConsoleWrite("$arrayScr10R=" & $arrayScr[10][0] & @CRLF)

	;########################################################################################################################

;~ 	For $i = 0 To 1 ;$BMP2Height - 1

	$searchFor = StringMid($BMP2Data, 1, $BMP1Width * 6);�������� ������ ����� �������� �� ������� ��������
	$k = 0
	$end = 0
	While $end == 0
		$tmp_str = hexToRGB(StringMid($searchFor, 1, 6)) ;����������� ������ ������� ������� ��������
		ConsoleWrite("$BMP1Width=" & $BMP1Width & @CRLF)
		ConsoleWrite("$BMP1Height=" & $BMP1Height & @CRLF)
		ConsoleWrite("$BMP2Width=" & $BMP2Width & @CRLF)
		ConsoleWrite("$BMP2Height=" & $BMP2Height & @CRLF)
		For $k = $k To $aScrlenght - 1 Step 1 ;��������� ���� � ������� ���������� ������� �������------------------------------------------------------------------


			If Abs($arrayScr[$k][0] - $tmp_str[0]) <= $limit And Abs($arrayScr[$k][1] - $tmp_str[1]) <= $limit And Abs($arrayScr[$k][2] - $tmp_str[2]) <= $limit Then ;���� ������� ������� �������� ����� �� ������� ���������
				$iPos = $k ;������������� �������� �� ������� ����� ������ �������
				$fX =  $k - ($BMP1Width * Int($k / ($BMP1Width)))
			    $fY = Int($k / $BMP1Width)
				ConsoleWrite("--> 1 fnd " & $k & " =" & $tmp_str[0] & "-" & $tmp_str[1] & "-" & $tmp_str[2] & "  find x=" &$fX & " y=" & $fY & @CRLF)
				;���������� ������� ���������, ������ ������ �� ��������
				For $j = 1 To $BMP2Width - 1 Step 1;��������� ���� ��� ������� ������� ������� ��������, ������� �� �������
					$tmp_str = hexToRGB(StringMid($searchFor, ($j * 6) + 1, 6));����������� ��������� ������� ������� ��������
					$k += 1;��������� �� ��������� ������� ���������
					$scr_hex = hexToRGB(StringMid($BMP1Data, ($k * 6) + 1, 6))
					If Abs($arrayScr[$k][0] - $tmp_str[0]) <= $limit And Abs($arrayScr[$k][1] - $tmp_str[1]) <= $limit And Abs($arrayScr[$k][2] - $tmp_str[2]) <= $limit Then ;���� ������� ������� �������� ����� �� ������� ���������
						$fX =  $k - ($BMP1Width * Int($k / ($BMP1Width)))
			$fY = Int($k / $BMP1Width)
						ConsoleWrite("--> "&$j&" fnd " & $k & " =" & $tmp_str[0] & "-" & $tmp_str[1] & "-" & $tmp_str[2] & "  find x=" &$fX & " y=" & $fY & @CRLF)
						$end = 1
					Else
						$iPos = 0
						ExitLoop
					EndIf
				Next

			EndIf

		Next;------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
		If $k > $aScrlenght - 2 Then
			ConsoleWrite("Ne nashli " & $k & @CRLF)
			$end = 1
		EndIf

	WEnd


	ConsoleWrite("--> $coord_find =" & $iPos & @CRLF)





;~ 		;Look for all lines above if there is also a match
;~ 		;Not doing it for the lines below as speed is more important and risk of mismatch on lines below is small
;~ 		$iAbove = 1
;~ 		If $iPos > 0 Then
;~ 			$bMatchPossible = True
;~ 			$matchedLines = 1;As first found line is matched we start counting
;~ 			;Location of the match
;~ 			$foundAtTop = Int($iPos / $BMP1LineWidth) - $i

;~ 			$foundAtLeft = Int(Mod($iPos, $BMP1LineWidth) / $imgBytes)

;~ 		Else

;~ 			$bMatchPossible = False
;~ 			ExitLoop
;~ 		EndIf

;~ 		While (($i + $iAbove) <= ($BMP2Height - 1)) And ($bMatchPossible = True)
;~ 			$searchFor = StringMid($BMP2Data, 1 + (($i + $iAbove) * $BMP2LineWidth), ($BMP2LineWidth - $imgBytes))


;~ 			$aboveLine = StringMid($BMP1Data, $iPos + ($iAbove * $BMP1LineWidth), ($BMP2LineWidth - $imgBytes))

;~ 			If comparePicLine($aboveLine, $searchFor) = False Then
;~ 				$bMatchPossible = False
;~ 				;To remember the area with the best match
;~ 				If $matchedLines >= $HighestMatchingLines Then
;~ 					$HighestMatchingLines = $matchedLines

;~ 					;Best guess of location

;~ 					$foundAtTop = Int($iPos / $BMP1LineWidth);+ $i - $BMP2Height
;~ 					$bestMatchLine = Int($iPos / $BMP1LineWidth)
;~ 				EndIf
;~ 				ExitLoop
;~ 			EndIf
;~ 			$matchedLines = $matchedLines + 1
;~ 			$iAbove = $iAbove + $iFuzzyDist
;~ 		WEnd

;~ 		;If bMatchPossible is still true most likely we have found the bitmap
;~ 		If $bMatchPossible = True Then
;~ 			ExitLoop
;~ 		Else
;~ 			ConsoleWrite("i not matched " & $iPos & " " & $matchedLines & @CRLF)
;~ 		EndIf

;~ 	Next

	;Return an error if not found else return an array with all information
	If $bMatchPossible = False Then
		SetError(1, 0, 0)
	EndIf
	$result_tmp[1][0] = $bMatchPossible
	$result_tmp[1][1] = $matchedLines
	$result_tmp[1][2] = $foundAtLeft
	$result_tmp[1][3] = $foundAtTop
	$result_tmp[1][4] = $BMP2Width
	$result_tmp[1][5] = $BMP2Height
	$result_tmp[1][6] = $HighestMatchingLines
	$result_tmp[1][7] = $bestMatchLine
	Return $result_tmp
EndFunc   ;==>findBMP

Func hexToRGB($tmp_str)
	$arrayRGB[0] = Dec(StringMid($tmp_str, 1, 2)) ;b
	$arrayRGB[1] = Dec(StringMid($tmp_str, 3, 2));g
	$arrayRGB[2] = Dec(StringMid($tmp_str, 5, 2));r
	Return $arrayRGB
EndFunc   ;==>hexToRGB


Func comparePicLine($s1, $s2)
	Local $sLen, $iMatched
	If $s1 = $s2 Then
		Return True
	Else
		$iMatched = 0
		$sLen = StringLen($s1)
		For $tJ = 1 To $sLen
			If StringMid($s1, $tJ, 1) = StringMid($s2, $tJ, 1) Then
				$iMatched = $iMatched + 1
			Else
				If $bDebug = True Then
;~ START debugging
					FileWrite(@ScriptDir & "\COMPAREDLINES.TXT", "Debug mismatch pixel " & $tJ & "=" & Int($tJ / 4) & @CRLF)
				EndIf
			EndIf
		Next
		If ($iMatched / $sLen) > $cMatchLinePercentage Then
			Return True
		Else
			Return False
		EndIf
	EndIf
EndFunc   ;==>comparePicLine

Func GetImage($BMPFile, ByRef $BMPDataStart, ByRef $Width, ByRef $Height, ByRef $Stride, $nScr, $imgBytes = 3)
	Local $Scan0, $pixelData, $hbScreen, $pBitmap, $pBitmapCap, $handle

	; Load the bitmap to search in
	If $BMPFile = "SCREEN" Then
		$hbScreen = _ScreenCapture_Capture("", 0, 0, 150, 150, False)
		$pBitmap = _GDIPlus_BitmapCreateFromHBITMAP($hbScreen); returns memory bitmap
	Else
		If $nScr == 1 Then
			;try to get a handle
			$handle = WinGetHandle($BMPFile)
			If @error Then
				;Assume its an unknown handle so correct filename should be given
				$pBitmap = _GDIPlus_BitmapCreateFromFile($BMPFile)
			Else
				$hbScreen = _ScreenCapture_CaptureWnd("", $handle, 0, 0, -1, -1, False)
				$pBitmap = _GDIPlus_BitmapCreateFromHBITMAP($hbScreen); returns memory bitmap
			EndIf
		Else
			$pBitmap = $BMPFile
		EndIf
	EndIf

;~  24 bits (3 bytes) or 16 bits (2 bytes) comparison
	If ($imgBytes = 2) Then
		$BitmapData = _GDIPlus_BitmapLockBits($pBitmap, 0, 0, _GDIPlus_ImageGetWidth($pBitmap), _GDIPlus_ImageGetHeight($pBitmap), $GDIP_ILMREAD, $GDIP_PXF16RGB555)
;~      $BitmapData= _GDIPlus_BitmapLockBits($pBitmap, 0, 0, _GDIPlus_ImageGetWidth($pBitmap), _GDIPlus_ImageGetHeight($pBitmap), $GDIP_ILMREAD, $GDIP_PXF32ARGB)
	Else
		$BitmapData = _GDIPlus_BitmapLockBits($pBitmap, 0, 0, _GDIPlus_ImageGetWidth($pBitmap), _GDIPlus_ImageGetHeight($pBitmap), $GDIP_ILMREAD, $GDIP_PXF24RGB)
;~      $BitmapData= _GDIPlus_BitmapLockBits($pBitmap, 0, 0, _GDIPlus_ImageGetWidth($pBitmap), _GDIPlus_ImageGetHeight($pBitmap), $GDIP_ILMREAD, $GDIP_PXF32ARGB)
	EndIf

	If @error Then MsgBox(0, "", "Error locking region " & @error)

	$Stride = DllStructGetData($BitmapData, "Stride");Stride - Offset, in bytes, between consecutive scan lines of the bitmap. If the stride is positive, the bitmap is top-down. If the stride is negative, the bitmap is bottom-up.
	$Width = DllStructGetData($BitmapData, "Width");Image width - Number of pixels in one scan line of the bitmap.
	$Height = DllStructGetData($BitmapData, "Height");Image height - Number of scan lines in the bitmap.
	$PixelFormat = DllStructGetData($BitmapData, "PixelFormat");Pixel format - Integer that specifies the pixel format of the bitmap
	$Scan0 = DllStructGetData($BitmapData, "Scan0");Scan0 - Pointer to the first (index 0) scan line of the bitmap.
	$pixelData = DllStructCreate("ubyte lData[" & (Abs($Stride) * $Height - 1) & "]", $Scan0)
	$BMPDataStart = $BMPDataStart & DllStructGetData($pixelData, "lData")
	_GDIPlus_BitmapUnlockBits($pBitmap, $BitmapData)
	_GDIPlus_ImageDispose($pBitmap)
	_WinAPI_DeleteObject($pBitmap)

EndFunc   ;==>GetImage

