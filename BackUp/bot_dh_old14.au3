#include <ScreenCapture.au3>
#include <Date.au3>
#include <GUIConstantsEx.au3>
#include <WindowsConstants.au3>
#include 'Tesseract.au3'
#include 'Tesseract.au3'
#include 'findImg.au3'
#include 'findText.au3'
#include 'findBMP.au3'


_GDIPlus_Startup()
GUISetState()

Global $kyski = 0
Global $kyski_glava = 4
Global $rasa = ""
Global $num_incr_vert = 25
Global $score_min = 2300
Global $score_max = 3300
Global $res_max_x = 1366
Global $res_max_y = 760
Global $activ_sbor_kyski = 1
Global $BgActive = 0, $count_main = 0, $numFriend = 10, $SkyActive = 0
Global $array_friend[$numFriend]
$array_friend[1] = "MATRIX"
$array_friend[2] = "MATRIXReload"
$array_friend[3] = "Alienware"
$array_friend[4] = "Holyinquisite"
$array_friend[5] = "AcrapnH" ;AsgardII
$array_friend[6] = "Xcrapl:" ;Asgard
$array_friend[7] = ""
$array_friend[8] = ""
$array_friend[9] = ""
_InitInterface()
HotKeySet("{F7}", "_SkyActive")
HotKeySet("{F6}", "_SborActive")
HotKeySet("{F5}", "_BgActive")
HotKeySet("{F4}", "_Terminate")
HotKeySet("{F3}", "test")

Func test()
	$tmp = findImg("img/" & $res_max_x & "x" & $res_max_y & "/guild_cart_zayavka.bmp", "", 0, 0, $res_max_x, $res_max_y)
	If $tmp <> 0 Then
		ConsoleWrite("x:" & $tmp[1][2] & ", y:" & $tmp[1][3] & @CRLF)
	Else
		ConsoleWrite("net" & @CRLF)
	EndIf

EndFunc   ;==>test

Func _InitInterface()
	$win_x = 274
	$win_y = 300
	$MainWindow = GUICreate("DH ���", $win_x, $win_y, $res_max_x - $win_x, 0, -1, $WS_EX_TOPMOST)
	Global $LabelActiveKyski = GUICtrlCreateLabel('1', 10, 10, $win_x, 20)
	GUICtrlSetFont($LabelActiveKyski, 14, 700)
	Global $LabelActiveSky = GUICtrlCreateLabel('1', 10, 30, $win_x, 20)
	GUICtrlSetFont($LabelActiveSky, 14, 700)
	Global $LabelActiveBG = GUICtrlCreateLabel('1', 10, 50, $win_x, 20)
	GUICtrlSetFont($LabelActiveBG, 14, 700)

	GUISetState()
EndFunc   ;==>_InitInterface

Func _Terminate()
	Exit 0
EndFunc   ;==>_Terminate

Func _SborActive()
	If $kyski == 0 Then
		$kyski = 1
	Else
		$kyski = 0
	EndIf
EndFunc   ;==>_SborActive

Func _SkyActive()
	If $SkyActive == 0 Then
		$SkyActive = 1
	Else
		$SkyActive = 0
	EndIf
EndFunc   ;==>_SkyActive

Func _BgActive()
	If $BgActive == 0 Then
		$BgActive = 1
	Else
		$BgActive = 0
	EndIf
EndFunc   ;==>_BgActive

Func skyArena()


	Sleep(1000)
	MouseClick("left", 719, 194) ;sky
	Sleep(5000)
	If findImg("img/" & $res_max_x & "x" & $res_max_y & "/null_sky.bmp", "", 171, 67, 284, 98) <> 0 Then
		ConsoleWrite("--> Arena 0")
		MouseClick("left", 885, 80) ;exit
		Sleep(2000)
	Else
		Sleep(2000)
		MouseClick("left", 647, 558) ;koloda
		Sleep(2000)
		MouseClick("left", 585, 90) ;koloda 3
		Sleep(2000)
		MouseClick("left", 885, 80) ;exit
		Sleep(2000)
		MouseClick("left", 830, 507) ;fight

		For $i = 1 To 5 Step 1
			Sleep(2000)
			MouseClick("left", 510, 496) ;elit
			Sleep(10000)
			MouseClick("left", 854, 218) ;avto
			Sleep(2000)
			MouseClick("left", 853, 396) ;skip
			Sleep(3000)
			MouseClick("left", 559, 353) ;close win/lose
			Sleep(2000)
			MouseClick("left", 371, 447) ;buy no

		Next


		Sleep(2000)
		MouseClick("left", 885, 80) ;exit
		Sleep(2000)
		MouseClick("left", 647, 558) ;koloda
		Sleep(2000)
		MouseClick("left", 778, 93) ;koloda 4
		Sleep(2000)
		MouseClick("left", 885, 80) ;exit
		Sleep(2000)
		MouseClick("left", 885, 80) ;exit
	EndIf



EndFunc   ;==>skyArena

Func _LoadDh()
;~ 	If WinExists("������������ �����") <> 0 Then
	WinClose("������������") ;close team viewer
;~ 	MouseClick("left", 855, 443) ;close team viewer
;~ 	EndIf

	If WinExists("BlueStacks App") == 0 Then
		Run('"C:\Program Files (x86)\BlueStacks\HD-StartLauncher.exe"')
		Sleep(10000)
		_FindAndClick("DH link", "img/" & $res_max_x & "x" & $res_max_y & "/dh_logo.bmp")
		_FindAndClick("Start logo", "img/" & $res_max_x & "x" & $res_max_y & "/start_logo.bmp")
	EndIf
EndFunc   ;==>_LoadDh

Func _FindAndClick($log_str, $img_adr)
	ConsoleWrite("--> Start find&click " & $log_str & @CRLF)

	$coord = 0
	While $coord == 0
		$coord = findImg($img_adr, "", 0, 0, $res_max_x, $res_max_y)
		ConsoleWrite("Find " & $log_str & @CRLF)
		If $coord <> 0 Then
			While $coord <> 0
				ConsoleWrite("Click " & $log_str & " x:" & $coord[1][2] & " y:" & $coord[1][3] & @CRLF)
				MouseClick("left", $coord[1][2], $coord[1][3])
				Sleep(1000)
				$coord = findImg($img_adr, "", 0, 0, $res_max_x, $res_max_y)
			WEnd
			$coord = 1
			ConsoleWrite("--> END find&click " & $log_str & @CRLF)
		EndIf

		Sleep(1000)
		$count_main = $count_main + 1 ;zashita ot zavisanii
				If $count_main > 50 Then
					$coord = 1
					$count_main = 0
				EndIf
	WEnd
	$coord = 0
EndFunc   ;==>_FindAndClick

Func kyski()

	_LoadDh()

	WinActivate("Blue")
	If findImg("img/" & $res_max_x & "x" & $res_max_y & "/main_img.bmp", "", 0, 0, $res_max_x, $res_max_y) <> 0 Then



		If $activ_sbor_kyski == 1 Then
			_FindAndClick("Mein wnd Guild", "img/" & $res_max_x & "x" & $res_max_y & "/guild.bmp")
			$coord_guild_tab_2 = 0
			While $coord_guild_tab_2 == 0
				$coord_guild_tab_2 = findImg("img/" & $res_max_x & "x" & $res_max_y & "/guild_tab_2.bmp", "", 0, 0, $res_max_x, $res_max_y)
				If $coord_guild_tab_2 <> 0 Then
					MouseClick("left", $coord_guild_tab_2[1][2], $coord_guild_tab_2[1][3])
					ConsoleWrite("Click guild tab 2" & " x:" & $coord_guild_tab_2[1][2] & " y:" & $coord_guild_tab_2[1][3] & @CRLF)
				EndIf
				$count_main = $count_main + 1 ;zashita ot zavisanii
				If $count_main > 50 Then
					$coord_guild_tab_2 = 1
					$count_main = 0
				EndIf
			WEnd

;~ 			$kyski_glava = 5
			$coord_guild_glava = 0
			While $coord_guild_glava == 0
				$coord_guild_glava = findImg("img/" & $res_max_x & "x" & $res_max_y & "/guild_glava_" & $kyski_glava & ".bmp", "", 0, 0, $res_max_x, $res_max_y)
				If $coord_guild_glava <> 0 Then
					ConsoleWrite("Find glava " & $kyski_glava & " x:" & $coord_guild_glava[1][2] & " y:" & $coord_guild_glava[1][3] & @CRLF)
					$coord_enter = 0
					While $coord_enter == 0
						$coord_enter = findImg("img/" & $res_max_x & "x" & $res_max_y & "/guild_glava_enter.bmp", "", 0, 0, $res_max_x, $res_max_y)
						If $coord_enter <> 0 Then
							MouseClick("left", $coord_enter[1][2], $coord_guild_glava[1][3] + $num_incr_vert)
							ConsoleWrite("Click enter glava " & $kyski_glava & " x:" & $coord_enter[1][2] & " y:" & $coord_guild_glava[1][3] + 20 & @CRLF)
						EndIf
						Sleep(1000)
						$count_main = $count_main + 1 ;zashita ot zavisanii
						If $count_main > 50 Then
							$coord_enter = 1
							$count_main = 0
						EndIf
					WEnd
				EndIf
				If $coord_guild_glava == 0 Then
					MouseClickDrag($MOUSE_CLICK_LEFT, $coord_guild_tab_2[1][2], $coord_guild_tab_2[1][3] + 300, $coord_guild_tab_2[1][2], $coord_guild_tab_2[1][3] + 100) ;��������� ����
					Sleep(2000)
				EndIf
				$count_main = $count_main + 1 ;zashita ot zavisanii
				If $count_main > 50 Then
					$coord_guild_glava = 1
					$count_main = 0
				EndIf
			WEnd

			_FindAndClick("Click guild dobicha", "img/" & $res_max_x & "x" & $res_max_y & "/guild_dobicha.bmp")

			$guild_cart = "abb"
			$coord_guild_cart = 0
			While $coord_guild_cart == 0
				$coord_guild_cart = findImg("img/" & $res_max_x & "x" & $res_max_y & "/guild_cart_" & $guild_cart & ".bmp", "", 0, 0, $res_max_x, $res_max_y)
				If $coord_guild_cart <> 0 Then
					ConsoleWrite("Find cart " & $guild_cart & " x:" & $coord_guild_cart[1][2] & " y:" & $coord_guild_cart[1][3] & @CRLF)
					$coord_zayavka = 0
					While $coord_zayavka == 0
						$coord_zayavka = findImg("img/" & $res_max_x & "x" & $res_max_y & "/guild_cart_zayavka.bmp", "", 0, 0, $res_max_x, $res_max_y)
						ConsoleWrite("find zayavka" & @CRLF)
						If $coord_zayavka <> 0 Then
							MouseClick("left", $coord_zayavka[1][2], $coord_guild_cart[1][3] + $num_incr_vert)
							ConsoleWrite("Click zayavka " & $guild_cart & " x:" & $coord_zayavka[1][2] & " y:" & $coord_guild_cart[1][3] + 20 & @CRLF)
						EndIf
						Sleep(1000)
						$count_main = $count_main + 1 ;zashita ot zavisanii
						If $count_main > 50 Then
							$coord_zayavka = 1
							$count_main = 0
						EndIf
					WEnd
				EndIf
				If $coord_guild_cart == 0 Then
					MouseClickDrag($MOUSE_CLICK_LEFT, $coord_guild_tab_2[1][2], $coord_guild_tab_2[1][3] + 300, $coord_guild_tab_2[1][2], $coord_guild_tab_2[1][3] + 100) ;��������� ����
					Sleep(2000)
				EndIf
				$count_main = $count_main + 1 ;zashita ot zavisanii
				If $count_main > 50 Then
					$coord_guild_cart = 1
					$count_main = 0
				EndIf
			WEnd
			$coord_zayavka = 0
			$coord_guild_cart = 0
			$coord_enter = 0
			$coord_guild_glava = 0
			$coord_guild_tab_2 = 0

			Sleep(5000)
			$coord_no = findImg("img/" & $res_max_x & "x" & $res_max_y & "/no.bmp", "", 0, 0, $res_max_x, $res_max_y)
			If $coord_no <> 0 Then
				MouseClick("left", $coord_no[1][2], $coord_no[1][3])
			EndIf

			Sleep(1000)
			Send("{ESC}")
			Sleep(1000)
			Send("{ESC}")
			Sleep(1000)
			Send("{ESC}")
		EndIf
		WinClose("BlueStacks App")
		$count_main = 0

;~ 		Sleep(1000 * 60 * 15)

	Else
		$count_main += 1
		If $count_main > 30 Then
			WinClose("BlueStacks App")
		EndIf

		If findImg("img/" & $res_max_x & "x" & $res_max_y & "/main_exit.bmp", "", 0, 0, 1028, 728) <> 0 Then
			MouseClick("left", 754, 199)
		Else
			Send("{ESC}")
			Sleep(1000)
		EndIf
	EndIf


EndFunc   ;==>kyski


Func _BG()

	For $countClick = 1 To 6

		Sleep(1000)
		If $countClick == 1 Then
			MouseClick("left", 215, 353) ;karavan 1
		EndIf
		If $countClick == 2 Then
			MouseClick("left", 219, 451) ;karavan 2
		EndIf
		If $countClick == 3 Then
			MouseClick("left", 377, 348) ;karavan 3
		EndIf
		If $countClick == 4 Then
			MouseClick("left", 555, 346) ;karavan 4
		EndIf
		If $countClick == 5 Then
			MouseClick("left", 588, 416) ;karavan 5
		EndIf
		If $countClick == 6 Then
			MouseClick("left", 730, 378) ;karavan 6
		EndIf

		Sleep(1000)
		$dirScrEnemyName = "tmp_img/enemy_name.tif"
		_ScreenCapture_Capture($dirScrEnemyName, 475, 235, 610, 256, False)
		$name_karavan = findText($dirScrEnemyName)
		ConsoleWrite("--> name karavan = " & $name_karavan & @CRLF)

		$dirScrEnemyScore = "tmp_img/enemy_score.tif"
		_ScreenCapture_Capture($dirScrEnemyScore, 492, 268, 567, 289, False)
		$score_karavan = findText($dirScrEnemyScore)
		$tmp = $score_karavan
		$tmp = StringStripWS(StringReplace($tmp, "S", "5"), 8)
		$tmp = StringReplace($tmp, "Z", "2")
		$tmp = StringReplace($tmp, "B", "8")
		$tmp = StringReplace($tmp, "O", "0")
		$score_karavan = $tmp
		ConsoleWrite("Score karavan = " & $score_karavan & @CRLF)

		$dirScrEnemyKyr = "tmp_img/enemy_kyr.tif"
		_ScreenCapture_Capture($dirScrEnemyKyr, 386, 294, 446, 316, False)
		$kyr_karavan = findText($dirScrEnemyKyr)
		$tmp = $kyr_karavan
		$tmp = StringStripWS(StringReplace($tmp, "S", "5"), 8)
		$tmp = StringReplace($tmp, "Z", "2")
		$tmp = StringReplace($tmp, "B", "8")
		$tmp = StringReplace($tmp, "O", "0")
		$kyr_karavan = $tmp
		ConsoleWrite("Kyrier karavan = " & $kyr_karavan & @CRLF)
		$not_atack = 0
		For $countFriend = 1 To $numFriend - 1
			If $name_karavan == $array_friend[$countFriend] Then
				$not_atack = 1
				ConsoleWrite("Drujishe! Ne atakyem" & @CRLF)
			EndIf
		Next

		$MO_atack = ($score_karavan - ($kyr_karavan * 1000)) + 1000
		ConsoleWrite("MO = " & $MO_atack & @CRLF)
		If $not_atack == 0 And $MO_atack > $score_min And $MO_atack < $score_max Then
			MouseClick("left", 385, 461) ;atack!
			ConsoleWrite("Atack!!! " & $name_karavan & @CRLF)
			$countClick = 1
			_AtackBg()
		Else
			Sleep(1000)
			If findImg("img/" & $res_max_x & "x" & $res_max_y & "/kyr_select.bmp", "", 494, 421, 732, 485) <> 0 Then
				ConsoleWrite("skip" & @CRLF)
				MouseClick("left", 620, 461) ;close select
			EndIf
			If $countClick == 6 Then
				Sleep(1000)
				MouseClick("left", 863, 450) ;refresh
				Sleep(2000)
			EndIf
		EndIf
	Next
EndFunc   ;==>_BG

Func _AtackBg()
	ConsoleWrite($rasa & @CRLF)
	Sleep(500)
	MouseClick("left", 423, 361) ;koloda
	Sleep(200)
	If $rasa == "" Then
		$rasa = "human"
	EndIf

	MouseClick("left", 781, 359) ;filter
	Sleep(200)
	MouseClick("left", 724, 356) ;po yrovnu
	Sleep(200)
	If $rasa == "human" Then
		MouseClick("left", 368, 229) ;human
		Sleep(200)
	EndIf
	If $rasa == "elf" Then
		MouseClick("left", 382, 292)
		Sleep(200)
	EndIf
	If $rasa == "nean" Then
		MouseClick("left", 375, 358)
		Sleep(200)
	EndIf
	If $rasa == "morti" Then
		MouseClick("left", 385, 418)
		Sleep(200)
	EndIf
	MouseClick("left", 657, 568) ;filter ok
	Sleep(200)
	If findImg("img/" & $res_max_x & "x" & $res_max_y & "/death_kart.bmp", "", 179, 544, 254, 587) == 0 Then
		MouseClick("left", 210, 566) ;click kart 1
		Sleep(100)
		MouseClick("left", 328, 566) ;click kart 2
		Sleep(100)
		MouseClick("left", 453, 566) ;click kart 3
		Sleep(100)
		MouseClick("left", 573, 566) ;click kart 4
		Sleep(100)
		MouseClick("left", 689, 566) ;click kart 5
		Sleep(100)
		MouseClick("left", 809, 566) ;click kart 6
		Sleep(100)
		MouseClickDrag($MOUSE_CLICK_LEFT, 826, 495, 196, 470) ;��������� vpravo
		Sleep(100)
		MouseClick("left", 210, 566) ;click kart 1
		Sleep(100)
		MouseClick("left", 328, 566) ;click kart 2
		Sleep(100)
		MouseClick("left", 453, 566) ;click kart 3
		Sleep(100)
		MouseClick("left", 573, 566) ;click kart 4
		Sleep(100)
	EndIf
	MouseClick("left", 578, 360) ;hero
	Sleep(500)
	MouseClick("left", 781, 359) ;filter
	Sleep(200)
	If $rasa == "human" Then
		MouseClick("left", 368, 229) ;human
	EndIf
	If $rasa == "elf" Then
		MouseClick("left", 382, 292)
	EndIf
	If $rasa == "nean" Then
		MouseClick("left", 375, 358)
	EndIf
	If $rasa == "morti" Then
		MouseClick("left", 385, 418)
	EndIf
	Sleep(100)
	MouseClick("left", 657, 568) ;filter ok
	Sleep(500)
	MouseClick("left", 210, 566) ;click kart 1
	Sleep(100)
	MouseClick("left", 824, 187) ;v atacky!
;~ 	ConsoleWrite("v atacky!" & @CRLF)
	Sleep(200)
	If findImg("img/" & $res_max_x & "x" & $res_max_y & "/ok_v_centre.bmp", "", 384, 431, 651, 498) <> 0 Then
		MouseClick("left", 501, 460) ;ok
		Sleep(200)
		If $rasa == "human" Then
			$rasa = "elf"
		Else
			If $rasa == "elf" Then
				$rasa = "nean"
			Else
				If $rasa == "nean" Then
					$rasa = "morti"
				Else
					If $rasa == "morti" Then
						$rasa = "human"
					EndIf
				EndIf
			EndIf
		EndIf
		MouseClick("left", 884, 81) ;close win/lose
		Sleep(2000)
	Else
		Sleep(2000)
		If findImg("img/" & $res_max_x & "x" & $res_max_y & "/no_celey1.bmp", "", 454, 381, 599, 416) == 0 Then
			Sleep(15000)
			MouseClick("left", 854, 218) ;avto
			Sleep(2000)
			MouseClick("left", 853, 396) ;skip
			Sleep(3000)
			MouseClick("left", 884, 81) ;close win/lose
			Sleep(2000)
		Else
			MouseClick("left", 854, 218) ;prst
		EndIf
	EndIf

EndFunc   ;==>_AtackBg

While 1
	If $kyski == 1 Then
		GUICtrlSetData($LabelActiveKyski, "Shard on")
	Else
		GUICtrlSetData($LabelActiveKyski, "Shard off")
	EndIf
	If $SkyActive == 1 Then
		GUICtrlSetData($LabelActiveSky, "SkyArena on")
	Else
		GUICtrlSetData($LabelActiveSky, "SkyArena off")
	EndIf
	If $BgActive == 1 Then
		GUICtrlSetData($LabelActiveBG, "BG on")
	Else
		GUICtrlSetData($LabelActiveBG, "BG off")
	EndIf
	If $BgActive == 1 Then
		_BG()
	EndIf
	If $SkyActive == 1 Then
		skyArena()
	EndIf

	If $kyski == 1 Then
		kyski()
	EndIf

	Sleep(300)
WEnd
