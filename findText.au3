Func findText($dir)
	_TessStart()
	$hTess = _TessBaseAPICreate()
	If @error Then
		ConsoleWrite(StringFormat('--> ERROR Base API Create\n'))
	EndIf
	_TessBaseAPIInit3($hTess)
;~         _TessBaseAPIInit3($hTess, '', 'rus')
	If @error Then
		ConsoleWrite(StringFormat('--> ERROR initialising tesseract\n'))
	EndIf

	$hPix = _PixRead($dir)
	_TessBaseAPISetImage2($hTess, $hPix)
	_TessBaseAPIRecognizet($hTess)
	$tmp = _TessBaseAPIGetUTF8Text($hTess)
	$tmp = StringStripWS($tmp, 8)
	_TessBaseAPIEnd($hTess)
	_TessBaseAPIDelete($hTess)
	_PixFreeData($hPix)
	Return $tmp
EndFunc   ;==>findText